# Optimize Shortcodes #

This plugin is a library of common shortcodes used by optimize worldwide



### Installation Instructions ###

* clone this repository into your plugins directory
* activate the plugin in the WordPress Dashboard

### Contribution guidelines ###

* create a pull request to this repository before modifying
* let the web team review your modifications
* Once your changes are approved they will be committed to the master branch

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact